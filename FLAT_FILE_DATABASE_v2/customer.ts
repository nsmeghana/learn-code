import read = require("./node_modules/readline-sync");

export class customer {
    ID: string;
    name: string;
    address: string;

    getCustomerInfo() {
        this.name = read.question('Enter name: ');
        this.address = read.question('Enter address: ');
    }

    selectCustomerProperty() {
        let selectedProperty = read.question('Select a property :\n1. ID \n2. Name \n3. Address \n');
        switch(selectedProperty)
        {
            case "1": return "ID";
            case "2": return "name";
            case "3": return "address";
            default: break;
        }
    }
}