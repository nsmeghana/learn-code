"use strict";
exports.__esModule = true;
var read = require("./node_modules/readline-sync");
var customer = /** @class */ (function () {
    function customer() {
    }
    customer.prototype.getCustomerInfo = function () {
        this.name = read.question('Enter name: ');
        this.address = read.question('Enter address: ');
    };
    customer.prototype.selectCustomerProperty = function () {
        var selectedProperty = read.question('Select a property :\n1. ID \n2. Name \n3. Address \n');
        switch (selectedProperty) {
            case "1": return "ID";
            case "2": return "name";
            case "3": return "address";
            default: break;
        }
    };
    return customer;
}());
exports.customer = customer;
