"use strict";
exports.__esModule = true;
var fs = require("fs");
var read = require("./node_modules/readline-sync");
var dbManager = /** @class */ (function () {
    function dbManager() {
    }
    dbManager.prototype.loadContent = function (recordType) {
        var fileContent = fs.readFileSync(recordType + '.json');
        return JSON.parse(fileContent.toString());
    };
    dbManager.prototype.saveRecord = function (recordObj, recordType) {
        var recordInfo = JSON.stringify(recordObj);
        fs.writeFileSync(recordType + '.json', recordInfo);
    };
    dbManager.prototype.searchRecord = function (recordObject, selectedProperty, propertyValue) {
        var objIndex = 0;
        var searchResult = [];
        while (objIndex < recordObject.length) {
            var object = recordObject[objIndex];
            if (propertyValue === object[selectedProperty])
                searchResult.push(object);
            objIndex++;
        }
        return searchResult;
    };
    dbManager.prototype.displayRecord = function (recordObject) {
        recordObject.forEach(function (element) {
            var requiredObject = Object.keys(element).map(function (key) { return element[key]; });
            var requiredObjectDetails = requiredObject.join("\n");
            console.log(requiredObjectDetails);
        });
    };
    dbManager.prototype.deleteRecord = function (recordObject, selectedProperty, propertyValue) {
        var _this = this;
        var requiredObjects = this.searchRecord(recordObject, selectedProperty, propertyValue);
        var deletedRecords = [];
        if (requiredObjects.length == 1) {
            var deletedRecordIndex = recordObject.indexOf(requiredObjects[0]);
            deletedRecords.push(recordObject.splice(deletedRecordIndex, 1));
        }
        else if (requiredObjects.length > 1) {
            console.log('Multiple records found.');
            this.displayRecord(requiredObjects);
            var deleteNumber = read.question("1. Delete one\n2. Delete all\n");
            switch (deleteNumber) {
                case "1":
                    var deleteID = read.question("\nSpecify ID: ");
                    this.deleteRecord(recordObject, 'ID', deleteID);
                    break;
                case "2":
                    requiredObjects.forEach(function (element) {
                        _this.deleteRecord(recordObject, 'ID', element.ID);
                    });
                    break;
                default: console.log('none');
            }
        }
        return deletedRecords;
    };
    dbManager.prototype.modifyRecord = function (recordObject, selectedProperty, propertyValue) {
        var _this = this;
        var requiredObjects = this.searchRecord(recordObject, selectedProperty, propertyValue);
        var modifiedRecords = [];
        if (requiredObjects.length == 1) {
            var modifiedRecordIndex = recordObject.indexOf(requiredObjects[0]);
            console.log(modifiedRecordIndex);
            var newValue = read.question('Enter new value: ');
            recordObject[modifiedRecordIndex][selectedProperty] = newValue;
            modifiedRecords.push(recordObject[modifiedRecordIndex]);
        }
        else if (requiredObjects.length > 1) {
            console.log('Multiple records found.');
            this.displayRecord(requiredObjects);
            var modifyNumber = read.question("1. Modify one\n2. Modify all\n");
            switch (modifyNumber) {
                case "1":
                    var modifyID = read.question("\nSpecify ID: ");
                    this.modifyRecord(recordObject, 'ID', modifyID);
                    break;
                case "2":
                    requiredObjects.forEach(function (element) {
                        _this.modifyRecord(recordObject, 'ID', element.ID);
                    });
                    break;
                default: console.log('none');
            }
        }
        return modifiedRecords;
    };
    return dbManager;
}());
exports.dbManager = dbManager;
