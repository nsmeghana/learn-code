import fs = require('fs')
import read = require("./node_modules/readline-sync");

export class dbManager {

    loadContent(recordType) {
        let fileContent = fs.readFileSync(recordType + '.json');
        return JSON.parse(fileContent.toString());
    }

    saveRecord(recordObj, recordType) {
        let recordInfo = JSON.stringify(recordObj);
        fs.writeFileSync(recordType + '.json', recordInfo);
    }

    searchRecord(recordObject, selectedProperty, propertyValue) {
        let objIndex = 0;
        let searchResult = [];
        while (objIndex < recordObject.length) {
            let object = recordObject[objIndex];
            if (propertyValue === object[selectedProperty])
                searchResult.push(object);
            objIndex++;
        }
        return searchResult;
    }

    displayRecord(recordObject) {
        recordObject.forEach(element => {
            let requiredObject = Object.keys(element).map(key => element[key]);
            let requiredObjectDetails = requiredObject.join("\n");
            console.log(requiredObjectDetails);
        });
    }

    deleteRecord(recordObject, selectedProperty, propertyValue) {
        let requiredObjects = this.searchRecord(recordObject, selectedProperty, propertyValue);
        let deletedRecords = [];
        if(requiredObjects.length == 1) {
            let deletedRecordIndex = recordObject.indexOf(requiredObjects[0]);
            deletedRecords.push(recordObject.splice(deletedRecordIndex, 1));
        }
        else if(requiredObjects.length > 1) {
            console.log('Multiple records found.')
            this.displayRecord(requiredObjects);
            let deleteNumber = read.question("1. Delete one\n2. Delete all\n");
            switch(deleteNumber) {
                case "1": let deleteID = read.question("\nSpecify ID: ");
                          this.deleteRecord(recordObject, 'ID', deleteID);
                          break;
                case "2": requiredObjects.forEach(element => {
                    this.deleteRecord(recordObject, 'ID', element.ID);
                });
                break;
                default: console.log('none');
            }
        }
        return deletedRecords;
    }

    modifyRecord(recordObject, selectedProperty, propertyValue) {
        let requiredObjects = this.searchRecord(recordObject, selectedProperty, propertyValue);
        let modifiedRecords = [];
        if(requiredObjects.length == 1) {
            let modifiedRecordIndex = recordObject.indexOf(requiredObjects[0]);
            console.log(modifiedRecordIndex);
            let newValue = read.question('Enter new value: ');
            recordObject[modifiedRecordIndex][selectedProperty] = newValue;
            modifiedRecords.push(recordObject[modifiedRecordIndex])
        }
        else if(requiredObjects.length > 1) {
            console.log('Multiple records found.')
            this.displayRecord(requiredObjects);
            let modifyNumber = read.question("1. Modify one\n2. Modify all\n");
            switch(modifyNumber) {
                case "1": let modifyID = read.question("\nSpecify ID: ");
                          this.modifyRecord(recordObject, 'ID', modifyID);
                          break;
                case "2": requiredObjects.forEach(element => {
                    this.modifyRecord(recordObject, 'ID', element.ID);
                });
                break;
                default: console.log('none');
            }
        }
        return modifiedRecords;
    }
}



