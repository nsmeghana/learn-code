"use strict";
exports.__esModule = true;
var read = require("./node_modules/readline-sync");
var DBManager_1 = require("./DBManager");
var customer_1 = require("./customer");
var fs = require("fs");
var customerRecords = [];
var db = new DBManager_1.dbManager;
function doesFileExist(recordType) {
    var fileName = (recordType + '.json');
    if (!fs.existsSync(fileName))
        fs.writeFileSync(fileName, '[]');
}
function isFileEmpty(recordType) {
    var fileContent = fs.readFileSync(recordType + '.json');
    if (fileContent.length == 2)
        return true;
}
function selectCategory() {
    var objType = read.question("Choose category:\n1. Customer\n");
    switch (objType) {
        case "1": return "customer";
        default:
            console.log('Invalid choice');
            break;
    }
}
function selectProperty(recordType) {
    var selectedProperty;
    switch (recordType) {
        case "customer":
            var customerObj = new customer_1.customer;
            selectedProperty = customerObj.selectCustomerProperty();
            return selectedProperty;
        default:
            console.log('Invalid');
            break;
    }
}
function getNextID(recordObject) {
    console.log(typeof recordObject);
    if (recordObject.length == 0)
        return 1;
    else
        return +recordObject[recordObject.length - 1].ID + 1;
}
function addRecord() {
    var recordType = selectCategory();
    switch (recordType) {
        case "customer":
            addCustomer();
            break;
        default:
            console.log('Invalid');
            break;
    }
}
function addCustomer() {
    var customerObj = new customer_1.customer;
    var id = getNextID(customerRecords);
    customerObj.ID = JSON.stringify(id);
    customerObj.getCustomerInfo();
    customerRecords.push(customerObj);
}
function retrieveRecord() {
    var recordType = selectCategory();
    switch (recordType) {
        case "customer":
            findCustomer();
            break;
        default:
            console.log('Invalid');
            return;
    }
}
function findCustomer() {
    var selectedProperty = selectProperty('customer');
    var propertyValue = read.question("Enter the value :\n");
    var searchResult = db.searchRecord(customerRecords, selectedProperty, propertyValue);
    if (searchResult.length > 0) {
        console.log('Search results:');
        db.displayRecord(searchResult);
    }
    else
        console.log('Record(s) not found');
}
function deleteRecord() {
    var recordType = selectCategory();
    switch (recordType) {
        case "customer":
            deleteCustomer();
            break;
        default:
            console.log('Invalid');
            return;
    }
}
function deleteCustomer() {
    var selectedProperty = selectProperty('customer');
    var propertyValue = read.question("Enter the value :\n");
    var deletedObjects = db.deleteRecord(customerRecords, selectedProperty, propertyValue);
    if (deletedObjects.length > 0) {
        console.log('Deleted:');
        db.displayRecord(deletedObjects);
    }
    else
        console.log('No object deleted');
}
function modifyRecord() {
    var recordType = selectCategory();
    switch (recordType) {
        case "customer":
            editCustomerInfo();
            break;
        default:
            console.log('Invalid');
            return;
    }
}
function editCustomerInfo() {
    var selectedProperty;
    var choice = read.question('Select property:\n1. Name\n2. Address\n');
    switch (choice) {
        case "1":
            selectedProperty = "name";
            break;
        case "2":
            selectedProperty = "address";
            break;
        default:
            console.log('Invalid');
            break;
    }
    var propertyValue = read.question("Enter the value :\n");
    var modifiedObjects = db.modifyRecord(customerRecords, selectedProperty, propertyValue);
    if (modifiedObjects.length > 0) {
        console.log('Modified:');
        db.displayRecord(modifiedObjects);
    }
    else
        console.log('No object modified');
}
function main() {
    doesFileExist('customer');
    if (!isFileEmpty('customer'))
        customerRecords = db.loadContent('customer');
    var userChoice;
    while (1) {
        userChoice = read.question("Choose operation:\n1. Add record\n2. Retreive record\n3. Delete Record\n4. Modify Record\n5. Exit\n");
        switch (userChoice) {
            case "1":
                addRecord();
                break;
            case "2":
                retrieveRecord();
                break;
            case "3":
                deleteRecord();
                break;
            case "4":
                modifyRecord();
                break;
            case "5":
                db.saveRecord(customerRecords, "customer");
                return;
            default: console.log('Invalid input');
        }
    }
}
main();
