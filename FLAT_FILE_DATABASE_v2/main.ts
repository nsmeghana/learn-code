import read = require("./node_modules/readline-sync");
import { dbManager } from './DBManager';
import { customer } from './customer';
import fs = require('fs')

let customerRecords = [];
let db = new dbManager;

function doesFileExist(recordType) {
    let fileName = (recordType + '.json');
    if (!fs.existsSync(fileName))
        fs.writeFileSync(fileName, '[]');
}

function isFileEmpty(recordType) {
    let fileContent = fs.readFileSync(recordType + '.json');
    if (fileContent.length == 2)
        return true;
}

function selectCategory() {
    let objType = read.question("Choose category:\n1. Customer\n");
    switch (objType) {
        case "1": return "customer";

        default: console.log('Invalid choice');
            break;
    }
}

function selectProperty(recordType) {
    let selectedProperty;
    switch (recordType) {
        case "customer": let customerObj = new customer;
            selectedProperty = customerObj.selectCustomerProperty();
            return selectedProperty;

        default: console.log('Invalid');
            break;
    }
}

function getNextID(recordObject) {
    console.log(typeof recordObject);
    if (recordObject.length == 0)
        return 1;
    else
        return +recordObject[recordObject.length - 1].ID + 1;
}

function addRecord() {
    let recordType = selectCategory();
    switch (recordType) {
        case "customer": addCustomer();
            break;
        default: console.log('Invalid');
            break;
    }
}

function addCustomer() {
    let customerObj = new customer;
    let id = getNextID(customerRecords)
    customerObj.ID = JSON.stringify(id);
    customerObj.getCustomerInfo();
    customerRecords.push(customerObj);
}

function retrieveRecord() {
    let recordType = selectCategory();
    switch (recordType) {
        case "customer": findCustomer();
            break;

        default: console.log('Invalid');
            return;
    }
}

function findCustomer() {
    if (customerRecords.length != 0) {
        let selectedProperty = selectProperty('customer');
        let propertyValue = read.question("Enter the value :\n");
        let searchResult = db.searchRecord(customerRecords, selectedProperty, propertyValue);
        if (searchResult.length > 0) {
            console.log('Search results:');
            db.displayRecord(searchResult);
        }
        else
            console.log('Record(s) not found');
    }
    else
        console.log('Records empty');
}

function deleteRecord() {
    let recordType = selectCategory();
    switch (recordType) {
        case "customer": deleteCustomer();
            break;

        default: console.log('Invalid');
            return;
    }
}

function deleteCustomer() {
    if(customerRecords.length != 0) {
        let selectedProperty = selectProperty('customer');
        let propertyValue = read.question("Enter the value :\n");
        let deletedObjects = db.deleteRecord(customerRecords, selectedProperty, propertyValue);
        if (deletedObjects.length > 0) {
            console.log('Deleted:')
            db.displayRecord(deletedObjects);
        }
        else
            console.log('No object deleted');
    }
    else
        console.log('Records empty');    
}

function modifyRecord() {
    let recordType = selectCategory();
    switch (recordType) {
        case "customer": editCustomerInfo();
            break;

        default: console.log('Invalid');
            return;
    }
}

function editCustomerInfo() {
    if(customerRecords.length !=0) {
        let selectedProperty;
        let choice = read.question('Select property:\n1. Name\n2. Address\n');
        switch (choice) {
            case "1": selectedProperty = "name";
                break;
            case "2": selectedProperty = "address";
                break;
            default: console.log('Invalid');
                break;
        }
        let propertyValue = read.question("Enter the value :\n");
        let modifiedObjects = db.modifyRecord(customerRecords, selectedProperty, propertyValue);
        if (modifiedObjects.length > 0) {
            console.log('Modified:')
            db.displayRecord(modifiedObjects);
        }
        else
            console.log('No object modified');
    }
    else
        console.log('Records empty');
}

function main() {
    doesFileExist('customer');
    if (!isFileEmpty('customer'))
        customerRecords = db.loadContent('customer');

    let userChoice;
    while (1) {
        userChoice = read.question("Choose operation:\n1. Add record\n2. Retreive record\n3. Delete Record\n4. Modify Record\n5. Save changes\n6. Exit");
        switch (userChoice) {
            case "1": addRecord();
                break;
            case "2": retrieveRecord();
                break;
            case "3": deleteRecord();
                break;
            case "4": modifyRecord();
                break;
            case "5": db.saveRecord(customerRecords, "customer");
                return;
            case "6": return;

            default: console.log('Invalid Input');
        }
    }
}

main();