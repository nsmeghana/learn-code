//Learn and Code - Assignment 1
//THE MONK AND THE CHAMBER OF SECRETS

#include <iostream>
#include <queue>

using namespace std;

//number of spiders and number of iterations
int spiderCount, iterationCount; 

//queues to store the powers and positions of the spiders
queue<pair<int, int>> powerMain, powerTemp; 

void pushBackToQueue(pair<int, int> &maxPower)
{
    while(!powerTemp.empty())
    {
        pair<int, int> temp = powerTemp.front();
        if(temp != maxPower) 
        {
            if(temp.first > 0) 
            {
                temp.first -= 1;
            }
            powerMain.push(temp);
        }
        powerTemp.pop();
    }
}

void findPosition()
{
    for(int iteration = 0; iteration < iterationCount; iteration++) 
    {
        int position = 0;     
        pair<int, int> maxPower = make_pair(-1, 0);

        while((!powerMain.empty()) && (position < iterationCount)) 
        {
            powerTemp.push(powerMain.front());
            pair<int, int> tempPower = powerMain.front();
            if(maxPower.first < tempPower.first)
            {
                maxPower = tempPower;
            }
            powerMain.pop();
            position++;
        }

        //Output initial postion (in main queue) of maximum power
        cout << maxPower.second << " ";

        pushBackToQueue(maxPower);            
    }
}

int main()
{
        
    cin >> spiderCount >> iterationCount; 

    for(int position = 1; position <= spiderCount; position++)
    {
        int power;    
        cin >> power;
        powerMain.push(make_pair(power, position)); 
    }

    findPosition();

    cout << endl;
    
    return 0;
}